import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Modal,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './url';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/AntDesign';
import {useIsFocused} from '@react-navigation/native';
import AddData from './AddData';
import Axios from 'axios';

function Home({navigation}) {
  const [dataMobil, setDataMobil] = useState('');
  const isFocused = useIsFocused();
  const [showModal, setShowModal] = useState(false);
  const [showSecondModal, setShowSecondModal] = useState(false);
  const [selected, setSelected] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const bukaModal = () => {
    setShowModal(true);
  };

  const tutupModal = () => {
    setShowModal(!showModal);
  };

  const tutupModal2 = () => {
    setShowSecondModal(!showSecondModal);
  };

  const getDataMobil = async () => {
    setIsLoading(true);
    Axios.get(`${BASE_URL}mobil`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('res get mobil', response);
        setIsLoading(false);
        if (response.status === 200) {
          setDataMobil(response.data.items);
        } else {
          if (response.status === 401 || response.status === 402) {
            //jika response status 401, maka session user habis dan perlu login kembali
            //jika response status 402, maka ada data yang salah ketika dikirimkan ke server
            return false;
          }
          if (response.status === 500) {
            //jika response status 500, maka server sedang error
            return false;
          }
        }
      })
      .catch(error => {
        console.log('error get mobil', error);
      });
  };

  const deleteData = async () => {
    const body = [
      {
        _uuid: selected._uuid,
      },
    ];
    Axios.delete(`${BASE_URL}mobil`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
      data: body,
    })
      .then(response => {
        console.log('res delete mobil', response);
        if (response.status === 200) {
          setDataMobil(response.data.items);
          alert('Data Mobil berhasil dihapus');
          refreshData();
          tutupModal2();
        } else {
          if (response.status === 401 || response.status === 402) {
            //jika response status 401, maka session user habis dan perlu login kembali
            //jika response status 402, maka ada data yang salah ketika dikirimkan ke server
            return false;
          }
          if (response.status === 500) {
            //jika response status 500, maka server sedang error
            return false;
          }
        }
      })
      .catch(error => {
        console.log('error delete mobil', error);
      });
  };

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);

  const refreshData = () => {
    getDataMobil();
  };

  const convertCurrency = (nominal, currency) => {
    if (typeof nominal !== 'undefined' && nominal !== null) {
      let rupiah = '';
      const nominalref = nominal.toString().split('').reverse().join('');
      for (let i = 0; i < nominalref.length; i++) {
        if (i % 3 === 0) {
          rupiah += nominalref.substr(i, 3) + '.';
        }
      }

      if (currency) {
        return (
          currency +
          rupiah
            .split('', rupiah.length - 1)
            .reverse()
            .join('')
        );
      } else {
        return rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('');
      }
    } else {
      return '';
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      <FlatList
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            // onPress={() =>
            //   navigation.navigate('Detail', {
            //     mobileHeader: item.unitImage,
            //     mobileName: item.title,
            //     mobileKM: item.totalKM,
            //     mobilePrice: convertCurrency(item.harga, 'Rp. '),
            //   })
            // }
            onPress={() => {
              setSelected(item);
              setShowSecondModal(true);
            }}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{width: '90%', height: 100, resizeMode: 'contain'}}
                source={{uri: item.unitImage}}
              />
            </View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}> {item.title}</Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {convertCurrency(item.harga, 'Rp. ')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        onPress={() => bukaModal()}
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
      <AddData
        show={showModal}
        onClose={() => tutupModal()}
        onAddOrUpdate={refreshData}
      />
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={showSecondModal}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setShowSecondModal(!showSecondModal);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => setShowSecondModal(!showSecondModal)}
                  style={{
                    width: '10%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingBottom: 10,
                    paddingTop: 20,
                  }}>
                  <Icon1 name="arrowleft" size={20} color="#000" />
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: '#000',
                    paddingTop: 10,
                  }}>
                  {/* {dataMobil ? 'Ubah Data' : 'Tambah Data'} */}
                  Edit & Delete
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  padding: 15,
                }}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('EditData', selected)}
                  style={styles.btnAdd}>
                  <Text style={{color: '#fff', fontWeight: '600'}}>Edit</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => deleteData()}
                  style={styles.btnDel}>
                  <Text style={{color: '#fff', fontWeight: '600'}}>Delete</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
      <Modal visible={isLoading}>
        <View style={styles.modal}>
          <View style={styles.modalContent}>
            <ActivityIndicator size="large" color="#fff" />
            <Text style={{color: '#fff', size: 20, marginTop: 10}}>
              Loading
            </Text>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnDel: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  modalContent: {
    width: 150,
    paddingVertical: 30,
    backgroundColor: '#2E2E2E',
    borderRadius: 10,
    alignItems: 'center',
  },
});

export default Home;
